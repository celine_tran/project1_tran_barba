package movies.importer;

import java.util.ArrayList;
/**
 * Deduper will look at the ArrayList provided and remove any duplicate movies. If it finds a duplicate movie
 * it will merge two duplicate movies and adjust the source of the movie.
 *@author Maria Barba
 *@author C�line Tran
 *@version 2.0
 *@since   2020-11-05
 */

public class Deduper extends Processor {
/**
 * Deduper constructor will make a super call to the Processor constructor
 * will set the third value to false so it will read the first line of the file.
 * @param sourceDir 
 * @param outputDir
 */
	public Deduper(String sourceDir, String outputDir) {
		super(sourceDir, outputDir, false);
		
	}
   
	@Override
	/**
	 * The overridden process method will take as input an ArrayList of String,will split every line of it into Movie fields
	 * and will create a tempMovies ArrayList of Movie object. Then it will fill the merged movies ArrayList
	 * of movies if there is no duplicate, when a dplicate is found ,a new Movie object is created out of suplicate movies and added
	 * to mergedMovies ArrayList. Finally,the values of mergedMovies is converted with the toString method from Movie class to fill the deduppedMoves ArrayList 
	 * that is returned by the method.
	 * @param ArrayList<String> input
	 * @return ArrayList<String> deduppedMovies
	 */
	public ArrayList<String> process(ArrayList<String> input) {
		ArrayList<String> deduppedMovies = new ArrayList<String>();
		
		String validReleaseYear = "";
		String validName = "";
		String validRuntime = "";
		String validSource = "";
		ArrayList<Movie> tempMovies= new ArrayList<Movie>();
		ArrayList<Movie> mergedMovies= new ArrayList<Movie>();
		for (int i=0;i<input.size();i++) {
			//loops through the input ArrayList
			String phrase=input.get(i);
			System.out.println("phrase: "+phrase);
			
			String [] splitedMovie = input.get(i).split("\\t",-1);
		
			validReleaseYear=splitedMovie[0];
			//sets the release year 
			System.out.println("releaseyear: "+validReleaseYear);
			validName=splitedMovie[1];
			//sets the movie name
			System.out.println("name: "+validName);
			validRuntime=splitedMovie[2];
			//sets the runtime
			System.out.println("runtime: "+validRuntime);
			validSource=splitedMovie[3];
			//sets the movie source
			System.out.println("source: "+validSource);
			
			Movie oneMovie = new Movie(validReleaseYear,validName,validRuntime,validSource);
			System.out.println(oneMovie.toString()+"One movie");
			tempMovies.add(oneMovie);
			//will create a Movie object and add it to tempMovies array
		}
		for(int indx=0;indx<tempMovies.size();indx++) {
	
			if(!(mergedMovies.contains(tempMovies.get(indx)))){
				//in the case that we don't have a duplicate we will add
				//normally to the empty ArryaList of String
			mergedMovies.add(tempMovies.get(indx));
			}
			else {
			//otherwise, when we do find a duplicate movie,we will need to change the values
		    //of the movie,add it to the arrayList with no duplicates
				int indxTempMov=tempMovies.indexOf(tempMovies.get(indx));
				//want to get indx when we find a duplicate in tempMovies
				validReleaseYear=tempMovies.get(indx).getReleaseYear();
				//set the release year of the movie of the duplicate
				validName=tempMovies.get(indx).getName();
				//set the name of the movie using the duplicate
				validRuntime=tempMovies.get(indx).getRuntime();
				//set the runtime of the movie of the duplicate
				if(mergedMovies.get(indx).getSource()==tempMovies.get(indxTempMov).getSource()) {
					validSource	=tempMovies.get(indx).getSource();
					//if the src of the duplicate and the previous movie is the same, set the src
					//of the first movie
				}
				else {
					validSource	=tempMovies.get(indx).getSource()+ ";" + mergedMovies.get(indx).toString();
					//else, merge the two sources
				}
				Movie mergedMovie=new Movie(validReleaseYear,validName,validRuntime,validSource);
				//create a merged movie
				mergedMovies.set(indxTempMov, mergedMovie);
				//set the merged movie at the indx when we find a duplicate
			}
			
			
		}
	
        for(int i=0;i<mergedMovies.size();i++) {
        	// apply the toString.method while looping through the merged movies
        	//Array list to set our returning array list value of the method
        	deduppedMovies.add(mergedMovies.get(i).toString());
        }
       
			
		return deduppedMovies;
	}
}

