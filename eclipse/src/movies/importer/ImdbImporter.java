package movies.importer;

import java.util.ArrayList;
/**
 * ImdbImporter constructor takes as input the src direcory and output directory of the raw imbd file used for file processing 
 * makes a call to the constructor from the Processor class and sets the third value to true to ignore the first line of the raw file.
 * @param srcDir is the imbd folder
 * @param outputDir is the starndardFormat_IK folder
 */
public class ImdbImporter extends Processor {

	public ImdbImporter(String srcDir, String outputDir) {
		super(srcDir,outputDir,true);
	}
	
	@Override 
	/**
	 * Process method is overridden from the Processor class.Takes ArrayList<String> as input and returns an ArrayList<String> 
	 * of with the appropriate movie format and right movie fields.
	 * @param ArrayList<String> of unformatted Movie Strings
	 * @return ArrayList<String> of formatted Movie Strings
	 */
	public ArrayList<String> process(ArrayList<String> input){
		ArrayList<String> appropriateMovieFormat = new ArrayList<String>();
		for(int i=0; i<input.size() ; i++) {
			String[] splitFields=input.get(i).split("\\t",-1);
			//split file info according to lines
			String srcVal="imdb";
			String movieReleaseYear="";
			String movieName="";
			String movieRuntime=""; 
			if(splitFields.length==22) {
					movieName=splitFields[1];
					//set the name of the movie
					System.out.println(movieName+": Moviename" );
				
					movieReleaseYear=splitFields[3];
					//set the releaseYear of the movie
					System.out.println(movieReleaseYear+": movieReleaseYear");
				
					movieRuntime=splitFields[6];
					//set the runtime of the movie
					System.out.println(movieRuntime+": movieRuntime");
					//create a movie
					
					Movie createdMovie=new Movie(movieReleaseYear,movieName,movieRuntime,srcVal);
					appropriateMovieFormat.add(createdMovie.toString());
					//create a new movie after analyzing each line of Imdb and making sure to get only the required movie fields
					//add a String with the movie values in the appropriate format to an ArrayList 
			}//if the movie line has the same number of fields(21)
			else {
			continue;
			//if there isn't the right amount of fields,skip directly to next column of movie.
			}
		}
		return appropriateMovieFormat;
	}
}
