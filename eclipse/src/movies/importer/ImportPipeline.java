package movies.importer;

import java.io.IOException;
/**
 * ImportPipeline will create a processor[] and fill in with appropriate fields to processors to load feeds of both files, normalize them, validate them, and finally dedupe them to produce a single file.
 *@author Maria Barba
 *@author C�line Tran
 *@version 2.0
 *@since   2020-11-05
 */
public class ImportPipeline  {
	/**
	 * Main will create a Process[] with KaggleImporter,ImdbImporter,Normalizer,Validator,Deduper and call the processAll method on Process[]
	 * @throws IOException if no file is found
	 */
	public static void main(String[] args) throws IOException {
		Processor[] everythingToProcess = new Processor[5];
		everythingToProcess[0]=new KaggleImporter("C:\\Users\\celine_tran\\courses\\java310\\project1_tran_barba\\Kaggle","C:\\Users\\celine_tran\\courses\\java310\\project1_tran_barba\\standardFormat_IK");
		everythingToProcess[1]=new ImdbImporter("C:\\Users\\celine_tran\\courses\\java310\\project1_tran_barba\\Imdb","C:\\Users\\celine_tran\\courses\\java310\\project1_tran_barba\\standardFormat_IK");
		everythingToProcess[2]=new Normalizer("C:\\Users\\celine_tran\\courses\\java310\\project1_tran_barba\\standardFormat_IK","C:\\Users\\celine_tran\\courses\\java310\\project1_tran_barba\\normalizerOutput");
		everythingToProcess[3]=new Validator("C:\\Users\\celine_tran\\courses\\java310\\project1_tran_barba\\normalizerOutput","C:\\Users\\celine_tran\\courses\\java310\\project1_tran_barba\\validatorOutput");
		everythingToProcess[4]=new Deduper("C:\\Users\\celine_tran\\courses\\java310\\project1_tran_barba\\validatorOutput","C:\\Users\\celine_tran\\courses\\java310\\project1_tran_barba\\deduperOutput");
		processAll(everythingToProcess);
	}
	/**
	 * processAll will input the Process[] from the main and run the execute method on every element of the array
	 * @param myProcArr
	 * @throws IOException if no file is found
	 */
	public static void processAll(Processor[] myProcArr) throws IOException {
		for (Processor myTypeOfProcessor: myProcArr) {
			myTypeOfProcessor.execute();
	}

}
}
