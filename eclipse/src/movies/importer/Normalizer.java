package movies.importer;
import java.util.ArrayList;
/**
 * This class will take standardized txt files and normalize them accordingly.
 *@author C�line Tran 1938648
 *@version 1.0
 *@since   2020-11-05
 */
public class Normalizer extends Processor {
  /**
		 * Parameterized constructor for Normalizer extending Processor class
		 * @param sourceDir
		 * @param outputDir
		 */
  public Normalizer(String sourceDir, String outputDir) {
    super(sourceDir, outputDir, false);
  }

  /**
	 * Overrides method from Processor class. 
	 * Takes an ArrayList and returns a new ArrayList which has been normalized. 
	 * Title is had title in lower case and runtime has only the first word kept.
	 * @return normalizedMovies
	 * @param input
	 */
  @Override
  public ArrayList < String > process(ArrayList < String > input) {
    ArrayList < String > normalizedMovies = new ArrayList < String > ();

    //This first loop goes through the whole input ArrayList line by line.
    for (String info: input) {
      System.out.println("the info: " + info);
      //Splits the lines into String[] at each tabs, ignoring the spaces
      String[] splitedMovie = info.split("\\t", -1);

      //these field do not need to be normalized
      String releaseYear = splitedMovie[0];
      System.out.println("release year: " + releaseYear);
      String source = splitedMovie[3];
      System.out.println("source: " + source);
      
      //converting the title into all lowercase letters
      String normalizedName = splitedMovie[1].toLowerCase();
      System.out.println("name: " + normalizedName);
      
      //Keeping only the first word of runtime. We do this by splitting the String at every occurrence of a space. 
      String[] splitedWord = splitedMovie[2].split("\\s+");
      String firstWord = splitedWord[0];
      String normalizedRuntime = firstWord;
      System.out.println("runtime: " + normalizedRuntime);
      
      //creating an instance of movie with all the initialized variables 		
      Movie MovieNormalized = new Movie(releaseYear, normalizedName, normalizedRuntime, source);
      //converting MovieNormalized which is a movie type to a string and inserting it inside the ArrayList normalizedMovie.
      normalizedMovies.add(MovieNormalized.toString());
    }
    return normalizedMovies;
  }
}