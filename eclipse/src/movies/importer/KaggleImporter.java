package movies.importer;
import java.util.ArrayList;
/**
 * This class will standardize the raw Kaggle txt file to match it too what a movie should look like.
 *@author C�line Tran 1938648
 *@version 1.0
 *@since   2020-11-05
 */
public class KaggleImporter extends Processor {
  /**
	 * Parameterized constructor for KaggleImporter extending Processor class
	 * @param sourceDir
	 * @param outputDir
	 */
  public KaggleImporter(String sourceDir, String outputDir) {
    super(sourceDir, outputDir, true);
  }

  /**
	 * Override method from Processor class. 
	 * Takes an ArrayList and returns the only the release year,title,runtime and source, format matching the toString method.
	 * @return formatMatch
	 * @param input
	 */
  @Override
  public ArrayList < String > process(ArrayList < String > input) {
    ArrayList < String > formatMatch = new ArrayList < String > ();
    
    //creating String variable that will be stored in Movie object later on.
    String validReleaseYear = "";
    String validName = "";
    String validRuntime = "";
    String validSource = "Kaggle";
    
  //This first loop goes through the whole input ArrayList line by line.
    for (String info: input) {
      //Splits the lines into String[] at each tabs, ignoring the spaces.
      String[] splitedMovie = info.split("\\t", -1);
      System.out.println("parsed: " + info);;
      
      //skips the line if it does not have 21 columns.
      if (splitedMovie.length != 21) {
        continue;
      }
      
      //Initializes the previously created variables according to their respective positions.
      else {
        validRuntime = splitedMovie[13];
        System.out.println("runtime: " + validRuntime);

        validName = splitedMovie[15];
        System.out.println("title: " + validName);

        validReleaseYear = splitedMovie[20];
        System.out.println("release year: " + validReleaseYear);
        
        //creating an instance of movie with all the initialized variables.
        Movie validMovie = new Movie(validReleaseYear, validName, validRuntime, validSource);
        //converting the type movie to a string and inserting it inside the ArrayList formatMatch.
        formatMatch.add(validMovie.toString());
      }
    }
    return formatMatch;
  }
}