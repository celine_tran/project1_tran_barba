package movies.importer;

/**
 * This class Movie creates a Movie object with a releaseYear,name of the movie,runtime of the movie and the source link of the movie.
 *@author Maria Barba
 *@author C�line Tran
 *@version 1.0
 *@since   2020-10-29
 */

public class Movie {
private String releaseYear;
private String name;
private String runtime; 
private String source;
/**
 * Parameterized constructor for a movie object
 * @param releaseYear
 * @param name
 * @param runtime
 * @param source
 */
public Movie( String releaseYear,String name,String runtime,String source) {
this.releaseYear=releaseYear;
this.name=name;
this.runtime=runtime;
this.source=source;
}
/**
 * releaseYear getter
 * @return releaseYear 
 */
public String getReleaseYear() { 
    return this.releaseYear; 
} 
/**
 * name getter
 * @return name
 */
public String getName() { 
    return this.name; 
} 
/**
 * runtime getter
 * @return runtime
 */
public String getRuntime() { 
    return this.runtime; 
} 
/**
 * source getter
 * @return source
 */
public String getSource() { 
    return this.source; 
} 
/**
 * ToString returns results of getters separated by a tab
 */
public String toString() {
	
	 return (getReleaseYear() + "\t" + getName() + "\t" + getRuntime() +"\t" + getSource());
}

@Override
/**
 * The overridden equals method will take as input an object and check if it is of instance Movie
 * If the name,release year and the runtime of the movie is no more than 5 minutes apart, the movie
 * object is equal.
 * @input Object o
 * @return a true or false depending if the movie object is equal
 */
public boolean equals(Object o) {
	if (!(o instanceof Movie)) {
		return false;
	}
	
	Movie aMovie = (Movie)o;
	if(this.name == aMovie.getName() && this.releaseYear==aMovie.getReleaseYear() && (Math.abs(Integer.parseInt(this.runtime)-Integer.parseInt(aMovie.getRuntime()))<=5 && (Math.abs(Integer.parseInt(this.runtime)-Integer.parseInt(aMovie.getRuntime()))>=0))) {
		return true;
	}
	else {
	return false ;
}
}
}

