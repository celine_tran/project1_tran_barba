
package movies.importer;
import java.util.ArrayList;
/**
 * Validator will validate the output of Normalizer
 *@author Maria Barba
 *@version 2.0
 *@since   2020-11-05
 */
public class Validator extends Processor {
	/**
	 * Validator constructor will take as input a src and output directory for the file that will contain normalized Imdb and RottenTomatoes movies
	 * will call super on the Processor constructor so that it will read the first line of the src file
	 * @param srcDir
	 * @param outputDir
	 */
	public Validator(String srcDir, String outputDir) {
		super(srcDir,outputDir,false);
		//will keep the 1st line because we take the ImbdImporter result file
	}
	@Override 
	/**
	 * Overidden process method will validate each movie String from the src file with the input of an ArrayList<String> and
	 * return a validated ArrayList<String> of movie Strings
	 * @param ArrayList<String> input
	 * @return ArrayList<String> validated
	 */
	public ArrayList<String> process(ArrayList<String> input){
		ArrayList<String> validatedMovies = new ArrayList<String>();
		
			for(int i=0; i<input.size() ; i++) {
				String[] splitFields=input.get(i).split("\\t",-1);
				//split file info according to lines
				String srcVal="";
				String validatedReleaseYear="";
				String validatedMovieName="";
				String validatedMovieRuntime=""; 
				//takes the output file of ImbdImporter as src folder 
				if(splitFields.length==4) {
					
					if(checkValidData(splitFields[0],0) && checkValidData(splitFields[1],1) && checkValidData(splitFields[2],2)) {
							validatedReleaseYear=splitFields[0];
							//will get the validated release year of the movie
							validatedMovieName=splitFields[1];
							//will get the validated name of the movie
							validatedMovieRuntime=splitFields[2];
							//will get the validated runtime of the movie
							srcVal=splitFields[3];
							Movie validatedMovie=new Movie(validatedReleaseYear,validatedMovieName,validatedMovieRuntime,srcVal);
							//create a new movie after analyzing each line of Imdb and making sure to get only the required movie fields
							validatedMovies.add(validatedMovie.toString());	
							//if the fields of the input arrayList are not null or empty and release year,run 
							//time can be converted to a movie,we add the values to the validatedMovies ArrayList
							//from the input ArrayList
					}
					else {
						continue;
						//skip recreating a movie obj and adding it to new ListArray
						//if the number of fields is not 4 and/or if the title,release year,runtime are valid
						//also release year of the movie cannot be converted to integer
					}
					
				}
				

			}//loop through the input array list
		return validatedMovies;
	}

/**
 * checkValidateData will take as input a String field inside one of the movie Strings for process and the index
 * of the field that we provide in the String[] array to make sure the inputValue is not null or empty and the 
 * release year of the movie and the runtime can be converted to string. If these conditions aren't met, it will
 * return a boolean of value false, otherwise will return true.
 * @param inputValue String
 * @param indxVal
 * @return true or false depending if the conditions are met
 */
	public boolean checkValidData(String inputValue,int indxVal) {
		boolean defaultVal=false;
		if(!(inputValue==null ||inputValue=="") ) {
			try {
				//checks if releaseYear can be converted to number
				if(indxVal==0 || indxVal==2) {
					int testIntvalue=Integer.parseInt(inputValue); 
				}
			}	
			catch (NumberFormatException  e) {
				return defaultVal;
				//the releaseYear and the run time of the movie cannot be converted to a number
			}
			return true;
		}
		else {
			return defaultVal;
		}
	}
}



