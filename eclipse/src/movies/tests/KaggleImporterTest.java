package movies.tests;
import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import movies.importer.KaggleImporter;
/**
 * This test file will test the Kaggle processor method that is inside the KaggleImporter class.
 *@author C�line Tran 1938648
 *@version 1.0
 *@since   2020-11-05
 */
class KaggleImporterTest {
	
	@Test//this will test if the method gives the right line by comparing a movie expected output and its actual output
	void testKaggleProcessor() {
		String source = "C:\\Users\\celine_tran\\courses\\java310\\project1_tran_barba\\Kaggle";
		String destination = "C:\\Users\\celine_tran\\courses\\java310\\project1_tran_barba\\standardFormat_IK";
		KaggleImporter test1 = new KaggleImporter(source,destination);
		ArrayList<String> movieTest1 = new ArrayList<String>();
		movieTest1.add("Brett Granstaff\tDiahann Carroll\tLara Jean Chorostecki\tRoddy Piper\tT.J. McGibbon\tJames Preston Rogers\t"
				+ "\"The journey of a professional wrestler who becomes a small town pastor and moonlights as a masked vigilante fighting injustice. While facing a crisis at home and at the church, the pastor must evade the police and somehow reconcile his violent secret identity with his calling to be a pastor.\""
				+ "\tWarren P. Sonoda\tDirector Not Available\tDirector Not Available\tAction\tPG-13\t1/8/2016\t111 minutes\tFreestyle Releasing\tThe Masked Saint\tScott Crowell\tBrett Granstaff\tWriter Not Available\tWriter Not Available\t2016");
		ArrayList<String> resultArray = new ArrayList<String>();
		resultArray = test1.process(movieTest1);
		ArrayList<String> expectedArray = new ArrayList<String>();
		expectedArray.add("2016\tThe Masked Saint\t111 minutes\tKaggle");
		assertEquals(expectedArray,resultArray);
	}
	
	
	@Test//This will test if the method will skip the line if it has LESS columns than what it should have. 
	void kaggleImporterCorrectColumnsMinus() {
		String source = "C:\\Users\\celine_tran\\courses\\java310\\project1_tran_barba\\Kaggle";
		String destination = "C:\\Users\\celine_tran\\courses\\java310\\project1_tran_barba\\standardFormat_IK";
		KaggleImporter test2 = new KaggleImporter(source,destination);
		ArrayList<String> movieTest2 = new ArrayList<String>();
		//This String has 19 in length and not 21, therefore it should be skipped
		movieTest2.add("Johnny Depp\tGwyneth Paltrow\tEwan McGregor\tOlivia Munn\tJeff Goldblum\tPaul Bettany"
				+ "\t\"Juggling some angry Russians, the British Mi5, his impossibly leggy wife and an international terrorist, debonair art dealer and part time rogue Charlie Mortdecai (Johnny Depp) must traverse the globe armed only with his good looks and special charm in a race to recover a stolen painting rumored to contain the code to a lost bank account filled with Nazi gold. (c) Lionsgate\""
				+ "\tDavid Koepp\tDirector Not Available\tDirector Not Available\tAction\tR\t1/23/2015\t106 minutes\tLiongate Films\tMortdecai\tWriter Not Available\tWriter Not Available\t2015");
		movieTest2.add("Brett Granstaff\tDiahann Carroll\tLara Jean Chorostecki\tRoddy Piper\tT.J. McGibbon\tJames Preston Rogers\t"
				+ "\"The journey of a professional wrestler who becomes a small town pastor and moonlights as a masked vigilante fighting injustice. While facing a crisis at home and at the church, the pastor must evade the police and somehow reconcile his violent secret identity with his calling to be a pastor.\""
				+ "\tWarren P. Sonoda\tDirector Not Available\tDirector Not Available\tAction\tPG-13\t1/8/2016\t111 minutes\tFreestyle Releasing\tThe Masked Saint\tScott Crowell\tBrett Granstaff\tWriter Not Available\tWriter Not Available\t2016");
		ArrayList<String> resultArray = new ArrayList<String>();
		resultArray = test2.process(movieTest2);
		ArrayList<String> expectedArray = new ArrayList<String>();
		//only the second result should be kept 
		expectedArray.add("2016\tThe Masked Saint\t111 minutes\tKaggle");
		assertEquals(expectedArray,resultArray);
	}
	
	
	@Test////This will test if the method will skip the line if it has MORE columns than what it should have. 
	void kaggleImporterCorrectColumnsPlus() {
		String source = "C:\\Users\\celine_tran\\courses\\java310\\project1_tran_barba\\Kaggle";
		String destination = "C:\\Users\\celine_tran\\courses\\java310\\project1_tran_barba\\standardFormat_IK";
		KaggleImporter test3 = new KaggleImporter(source,destination);
		ArrayList<String> movieTest3 = new ArrayList<String>();
		
		movieTest3.add("Brett Granstaff\tDiahann Carroll\tLara Jean Chorostecki\tRoddy Piper\tT.J. McGibbon\tJames Preston Rogers\t"
				+ "\"The journey of a professional wrestler who becomes a small town pastor and moonlights as a masked vigilante fighting injustice. While facing a crisis at home and at the church, the pastor must evade the police and somehow reconcile his violent secret identity with his calling to be a pastor.\""
				+ "\tWarren P. Sonoda\tDirector Not Available\tDirector Not Available\tAction\tPG-13\t1/8/2016\t111 minutes\tFreestyle Releasing\tThe Masked Saint\tScott Crowell\tBrett Granstaff\tWriter Not Available\tWriter Not Available\t2016");
		//This has 24 Strings therefore, it should be skipped as it does not respect one of the constraint.
		movieTest3.add("ADDED STRING\tRobert Mitchum\tADDED STRING\tDeborah Kerr\tADDED STRING\tCast Not Available\tCast Not Available\tCast Not Available\tCast Not Available"
				+ "\t\"A two-person character study directed by John Huston, Heaven Knows Mr. Allison stars Robert Mitchum as a World War II Marine sergeant and Deborah Kerr as a Roman Catholic nun. Both nun and sergeant are marooned on a South Pacific island, hemmed in by surrounding Japanese troops. Mitchum does his best to make the nun's ordeal less painful, but is torn by his growing love for her. Kerr is equally fond of Mitchum, but refuses to renounce her vows. Their unrealized ardor mellows into mutual respect as they struggle to survive before help arrives. Based on a novel by Charles K. Shaw, Heaven Knows, Mr. Allison was coproduced by Eugene Frenke, who later filmed a low-budget variation on the story, The Nun and the Sergeant (62), which starred Frenke's wife Anna Sten. ~ Hal Erickson, Rovi\""
				+ "\tJohn Huston\tDirector Not Available\tDirector Not Available\tAction\tNR\t1/1/1957\t108 minutes\tFox\tHeaven Knows Mr. Allison\tJohn Lee Mahin\tJohn Huston\tWriter Not Available\tWriter Not Available\t1957");
		movieTest3.add("Jeremy Renner\tAnthony Mackie\tBrian Geraghty\tGuy Pearce\tRalph Fiennes\tDavid Morse"
				+ "\t\"Based on the personal wartime experiences of journalist Mark Boal (who adapted his experiences with a bomb squad into a fact-based, yet fictional story), director Kathryn Bigelow's Iraq War-set action thriller The Hurt Locker presents the conflict in the Middle East from the perspective of those who witnessed the fighting firsthand -- the soldiers. As an elite Army Explosive Ordnance Disposal team tactfully navigates the streets of present-day Iraq, they face the constant threat of death from incoming bombs and sharp-shooting snipers. In Baghdad, roadside bombs are a common danger. The Army is working to make the city a safer place for Americans and Iraqis, so when it comes to dismantling IEDs (improvised explosive devices) the Explosive Ordnance Disposal (EOD) crew is always on their game. But protecting the public isn't easy when there's no room for error, and every second spent dismantling a bomb is another second spent flirting with death. Now, as three fearless bomb technicians take on the most dangerous job in Baghdad, it's only a matter of time before one of them gets sent to \"\"the hurt locker.\"\" Jeremy Renner, Guy Pearce, and Ralph Fiennes star. ~ Jason Buchanan, Rovi\""
				+ "\tKathryn Bigelow\tDirector Not Available\tDirector Not Available\tAction\tR \t6/26/2009\t131 minutes\tSummit Entertainment\tThe Hurt Locker	Mark Boal\tWriter Not Available\tWriter Not Available\tWriter Not Available\t2009");
		ArrayList<String> resultArray = new ArrayList<String>();
		resultArray = test3.process(movieTest3);
		ArrayList<String> expectedArray = new ArrayList<String>();
		//only 2 result should be kept as the middle one is skip
		expectedArray.add("2016\tThe Masked Saint\t111 minutes\tKaggle");
		expectedArray.add("2009\tThe Hurt Locker\t131 minutes\tKaggle");
		assertEquals(expectedArray,resultArray);
	}

	
	@Test////This will test if the method will skip the lines that have LESS AND MORE columns than what it should have. 
	void kaggleImporterCorrectColumnsPlusMinus() {
		String source = "C:\\Users\\celine_tran\\courses\\java310\\project1_tran_barba\\Kaggle";
		String destination = "C:\\Users\\celine_tran\\courses\\java310\\project1_tran_barba\\standardFormat_IK";
		KaggleImporter test3 = new KaggleImporter(source,destination);
		ArrayList<String> movieTest3 = new ArrayList<String>();
				//This has 24 Strings therefore, it should be skipped as it does not respect one of the constraint.
				movieTest3.add("ADDED STRING\tRobert Mitchum\tADDED STRING\tDeborah Kerr\tADDED STRING\tCast Not Available\tCast Not Available\tCast Not Available\tCast Not Available"
						+ "\t\"A two-person character study directed by John Huston, Heaven Knows Mr. Allison stars Robert Mitchum as a World War II Marine sergeant and Deborah Kerr as a Roman Catholic nun. Both nun and sergeant are marooned on a South Pacific island, hemmed in by surrounding Japanese troops. Mitchum does his best to make the nun's ordeal less painful, but is torn by his growing love for her. Kerr is equally fond of Mitchum, but refuses to renounce her vows. Their unrealized ardor mellows into mutual respect as they struggle to survive before help arrives. Based on a novel by Charles K. Shaw, Heaven Knows, Mr. Allison was coproduced by Eugene Frenke, who later filmed a low-budget variation on the story, The Nun and the Sergeant (62), which starred Frenke's wife Anna Sten. ~ Hal Erickson, Rovi\""
						+ "\tJohn Huston\tDirector Not Available\tDirector Not Available\tAction\tNR\t1/1/1957\t108 minutes\tFox\tHeaven Knows Mr. Allison\tJohn Lee Mahin\tJohn Huston\tWriter Not Available\tWriter Not Available\t1957");
				movieTest3.add("Brett Granstaff\tDiahann Carroll\tLara Jean Chorostecki\tRoddy Piper\tT.J. McGibbon\tJames Preston Rogers\t"
				+ "\"The journey of a professional wrestler who becomes a small town pastor and moonlights as a masked vigilante fighting injustice. While facing a crisis at home and at the church, the pastor must evade the police and somehow reconcile his violent secret identity with his calling to be a pastor.\""
				+ "\tWarren P. Sonoda\tDirector Not Available\tDirector Not Available\tAction\tPG-13\t1/8/2016\t111 minutes\tFreestyle Releasing\tThe Masked Saint\tScott Crowell\tBrett Granstaff\tWriter Not Available\tWriter Not Available\t2016");
				//This line has 11 strings therefore, it should be skipped.
				movieTest3.add("\"Based on the personal wartime experiences of journalist Mark Boal (who adapted his experiences with a bomb squad into a fact-based, yet fictional story), director Kathryn Bigelow's Iraq War-set action thriller The Hurt Locker presents the conflict in the Middle East from the perspective of those who witnessed the fighting firsthand -- the soldiers. As an elite Army Explosive Ordnance Disposal team tactfully navigates the streets of present-day Iraq, they face the constant threat of death from incoming bombs and sharp-shooting snipers. In Baghdad, roadside bombs are a common danger. The Army is working to make the city a safer place for Americans and Iraqis, so when it comes to dismantling IEDs (improvised explosive devices) the Explosive Ordnance Disposal (EOD) crew is always on their game. But protecting the public isn't easy when there's no room for error, and every second spent dismantling a bomb is another second spent flirting with death. Now, as three fearless bomb technicians take on the most dangerous job in Baghdad, it's only a matter of time before one of them gets sent to \"\"the hurt locker.\"\" Jeremy Renner, Guy Pearce, and Ralph Fiennes star. ~ Jason Buchanan, Rovi\""
				+ "\tKathryn Bigelow\t6/26/2009\t131 minutes\tSummit Entertainment\tThe Hurt Locker	Mark Boal\tWriter Not Available\tWriter Not Available\tWriter Not Available\t2009");
		ArrayList<String> resultArray = new ArrayList<String>();
		resultArray = test3.process(movieTest3);
		ArrayList<String> expectedArray = new ArrayList<String>();
		//only 2 result should be kept as the middle one is skip.
		expectedArray.add("2016\tThe Masked Saint\t111 minutes\tKaggle");
		assertEquals(expectedArray,resultArray);
	}
	
	

}
