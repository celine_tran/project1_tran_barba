package movies.tests;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.*;
import java.io.IOException;
import java.util.ArrayList;

import org.junit.jupiter.api.Test;
import movies.importer.*;
/**
 * This class will test the process method of the Validator
 *@author Maria Barba
 *@version 2.0
 *@since   2020-11-05
 */
class ValidatorTest {

	@Test
	public void testProcessorCorrectVals()  {
		Validator imdbMoviesValidated =new Validator("normalizerOutput","validatorOutput");
		ArrayList<String> myResultArrNormalized = new ArrayList<String>();
		myResultArrNormalized.add("1913\tQuo Vadis?\t120\tnormalizerOutput");
		myResultArrNormalized.add("1906\tThe Story of the Kelly Gang\t70\tnormalizerOutput");
		myResultArrNormalized.add("1919\tMadame DuBarry\t85\tnormalizerOutput");
		ArrayList<String> myResultArrProcessed = new ArrayList<String>();
		myResultArrProcessed=imdbMoviesValidated.process(myResultArrNormalized);
		ArrayList<String> myExpectedArr = new ArrayList<String>();
		myExpectedArr.add("1913\tQuo Vadis?\t120\tnormalizerOutput");
		myExpectedArr.add("1906\tThe Story of the Kelly Gang\t70\tnormalizerOutput");
		myExpectedArr.add("1919\tMadame DuBarry\t85\tnormalizerOutput");
		assertArrayEquals(myExpectedArr.toArray(),myResultArrProcessed.toArray());	
		//will test if both the expected ArrayList result will match the actual ArrayList result
		//provided with the correct fields
	}
	@Test
	public void testProcessorNumberFormatExc()  {
		Validator imdbMoviesValidated =new Validator("normalizerOutput","validatorOutput");
		ArrayList<String> myResultArrNormalized = new ArrayList<String>();;
		myResultArrNormalized.add("1906year\tThe Story of the Kelly Gang\t70\tnormalizerOutput");
		myResultArrNormalized.add("1919\tMadame DuBarry\t85\tnormalizerOutput");
		ArrayList<String> myResultArrProcessed = new ArrayList<String>();
		try {
			myResultArrProcessed=imdbMoviesValidated.process(myResultArrNormalized);
		}
		catch (NumberFormatException  e) {
			// should catch the error if the movie release year or runtime cannot be converted to Int
			}
		ArrayList<String> myExpectedArr = new ArrayList<String>();
		myExpectedArr.add("1919\tMadame DuBarry\t85\tnormalizerOutput");
		assertArrayEquals(myExpectedArr.toArray(),myResultArrProcessed.toArray());	
		//will test if both the expected ArrayList result will match the actual ArrayList result
		//provided with the year of the first movie that cannot be converted to an Integer
	}
	@Test
	public void testProcessorNullVal()  {
		Validator imdbMoviesValidated =new Validator("normalizerOutput","validatorOutput");
		ArrayList<String> myResultArrNormalized = new ArrayList<String>();
		myResultArrNormalized.add("\t\tFrom the Manger to the Cross; or, Jesus of Nazareth\t60\tnormalizerOutput");
		myResultArrNormalized.add("1911\tL'Inferno\t68\tnormalizerOutput");
		//the name of the 1st movie is null and the release year of the third movie is null
		ArrayList<String> myResultArrProcessed = new ArrayList<String>();
		myResultArrProcessed=imdbMoviesValidated.process(myResultArrNormalized);
		ArrayList<String> myExpectedArr = new ArrayList<String>();
		myExpectedArr.add("1911\tL'Inferno\t68\tnormalizerOutput");
		assertArrayEquals(myExpectedArr.toArray(),myResultArrProcessed.toArray());
		//will test if both the expected ArrayList result will match the actual ArrayList result
		//provided with the year of the first movie is null
	}
	@Test
	public void testProcessorEmptyVal()  {
		Validator imdbMoviesValidated =new Validator("normalizerOutput","validatorOutput");
		ArrayList<String> myResultArrNormalized = new ArrayList<String>();
		myResultArrNormalized.add("1911\t\"\"\t53\tnormalizerOutput");
		myResultArrNormalized.add("1894\tMiss Jerry\t45\tnormalizerOutput");
		ArrayList<String> myResultArrProcessed = new ArrayList<String>();
		myResultArrProcessed=imdbMoviesValidated.process(myResultArrNormalized);
		ArrayList<String> myExpectedArr = new ArrayList<String>();
		myExpectedArr.add("1894\tMiss Jerry\t45\tnormalizerOutput");
		assertArrayEquals(myExpectedArr.toArray(),myResultArrProcessed.toArray());
		//will test if both the expected ArrayList result will match the actual ArrayList result
		//provided with an empty string for the name of the first movie
	}

}
