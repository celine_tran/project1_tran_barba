package movies.tests;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import movies.importer.Movie;
/**
 * This test file will test Movie getters and the toString method
 *@author Maria Barba
 *@author C�line Tran
 *@version 1.0
 *@since   2020-10-29
 */
class MovieTests {


	@Test 
	public void testtoString() {
		Movie movie0=new Movie("1994","Forest Gump","200", "Kaggle");
		String expectedOutput="1994	Forest Gump	200	Kaggle";
		String actualOutput = movie0.toString();
			assertEquals(expectedOutput, actualOutput);
		}
	
	@Test
	public void testreleaseYearGetter() {
	Movie movie1=new Movie("1999","The Lion King","90", "Imdb");
		String expectedYear="1999";
		String resultYear=movie1.getReleaseYear();	
		assertEquals(expectedYear, resultYear);
	}
	@Test
	public void testNameGetter() {
		Movie movie2=new Movie("1980","The Shining","98", "Kaggle");
		String expectedName="The Shining";
		String resultName=movie2.getName(); 	
		assertEquals(expectedName, resultName);
		
	}
	@Test
	public void testRuntimeGetter() {
		Movie movie3=new Movie("2019","Ready or Not","150", "Imdb");
		String expectedRuntime="150";
		String resultRuntime=movie3.getRuntime();	
		assertEquals(expectedRuntime, resultRuntime);
	}
	@Test
	public void testSourceGetter() {
		Movie movie4=new Movie("2019","Jumanji 3","120", "Kaggle");
		String expectedSource="Kaggle";
		String resultSource=movie4.getSource();	
		assertEquals(expectedSource, resultSource);
	}
			

}
