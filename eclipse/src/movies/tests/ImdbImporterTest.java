package movies.tests;
import java.io.IOException;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;
import static org.junit.Assert.assertArrayEquals;
import movies.importer.*;
/**
 * This class will test the process method from ImdbImporter
 *@author Maria Barba
 *@version 2.0
 *@since   2020-11-05
 */
class ImdbImporterTest {
	@Test
	public void testProcessor() throws IOException {
		ImdbImporter imdbMovies =new ImdbImporter("imdb","starndardFormat_IK");
		ArrayList<String> myResultArr = new ArrayList<String>();
		myResultArr.add("tt0002101\tCleopatra\tCleopatra\t1912\t11/13/1912\t \t100\t \t \t \t \t \t \t \t \t \t \t \t \t \t");
		myResultArr.add("tt0002199\tFrom the Manger to the Cross; or, Jesus of Nazareth\tFrom the Manger to the Cross; or, Jesus of Nazareth\t1912\t1913\t \t60\t \t \t \t \t \t \t \t \t \t \t \t \t \t");
		ArrayList<String> myResultArrProcessed = new ArrayList<String>();
		myResultArrProcessed=imdbMovies.process(myResultArr);
		ArrayList<String> myExpectedArr = new ArrayList<String>();
		myExpectedArr.add("1912	Cleopatra	100	imdb");
		myExpectedArr.add("1912	From the Manger to the Cross; or, Jesus of Nazareth	60	imdb");
		assertArrayEquals(myExpectedArr.toArray(),myResultArrProcessed.toArray());	
		//will test if both the expected ArrayList result will match the actual ArrayList result
		//provided with the correct fields
	}
	@Test
	public void testProcessorWithMissingField() throws IOException {
		ImdbImporter imdbMovies2 =new ImdbImporter("imdb","starndardFormat_IK");
		ArrayList<String> myResultArr = new ArrayList<String>();
		myResultArr.add("tt0002461\tThe Fortune\tThe Fortune\t10/15/1975\t \t90\t \t \t \t \t \t \t \t \t \t \t \t \t \t");
		myResultArr.add("tt0002130\tL'Inferno\tL'Inferno\t1911\t3/6/1911\t \t68\t \t \t \t \t \t \t \t \t \t \t \t \t \t");
		ArrayList<String> myResultArrProcessed = new ArrayList<String>();
		myResultArrProcessed=imdbMovies2.process(myResultArr);
		ArrayList<String> myExpectedArr = new ArrayList<String>();
		myExpectedArr.add("1911	L'Inferno	68	imdb");
		assertArrayEquals(myExpectedArr.toArray(),myResultArrProcessed.toArray());	
		//will test if both the expected ArrayList result will match the actual ArrayList result
		//provided with one String, missing the release year of the movie
	}
	@Test
	public void testProcessorWithEmptyField() throws IOException {
		ImdbImporter imdbMovies4 =new ImdbImporter("imdb","starndardFormat_IK");
		ArrayList<String> myResultArr = new ArrayList<String>();
		myResultArr.add("tt0002101\t\tCleopatra\t1912\t11/13/1912\t \t100\t \t \t \t \t \t \t \t \t \t \t \t \t \t");
		myResultArr.add("tt0002445\tQuo Vadis?\tQuo Vadis?\t1913\t3/1/1913\t \t120\t \t \t \t \t \t \t \t \t \t \t \t \t \t");
		ArrayList<String> myResultArrProcessed = new ArrayList<String>();
		myResultArrProcessed=imdbMovies4.process(myResultArr);
		ArrayList<String> myExpectedArr = new ArrayList<String>();
		myExpectedArr.add("1912	\"\"	100	imbd");
		myExpectedArr.add("1913 Quo Vadis? 120 imbd");
		assertArrayEquals(myExpectedArr.toArray(),myResultArrProcessed.toArray());
		//will test if both the expected ArrayList result will match the actual ArrayList result
		//provided with one String, with one empty field for the name of the movie
	}
	}


