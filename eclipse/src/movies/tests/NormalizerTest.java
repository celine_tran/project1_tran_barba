package movies.tests;
import static org.junit.jupiter.api.Assertions.*;
import movies.importer.Normalizer;
import java.util.ArrayList;
import org.junit.jupiter.api.Test;
/**
 * This test file will test the processor method of the Normalizer class.
 *@author C�line Tran 1938648
 *@version 1.0
 *@since   2020-11-05
 */
class NormalizerTest {

	@Test//This will test if the normalizer works properly.
	//It should return the title in lower case and only keep the first word in the runtime.
	void testNormalizerProcess() {
		String source = "C:\\Users\\celine_tran\\courses\\java310\\project1_tran_barba\\standardFormat_IK";
		String destination = "C:\\Users\\celine_tran\\courses\\java310\\project1_tran_barba\\normalizerOutput";
		Normalizer test1 = new Normalizer(source,destination);
		ArrayList<String> standardMovie = new ArrayList<String>();
		standardMovie.add("2008\tThe Mummy: Tomb of the Dragon Emperor\t112 minutes\tKaggle");
		
		ArrayList<String> resultArray = new ArrayList<String>();
		resultArray = test1.process(standardMovie);
		
		ArrayList<String> expectedArray = new ArrayList<String>();
		expectedArray.add("2008\tthe mummy: tomb of the dragon emperor\t112\tKaggle");
		assertEquals(expectedArray,resultArray);
	}
	
	@Test//this will test will test the normalizer lowercase with different test case.
	void NormalizeLowerCaseTest() {
		String source = "C:\\Users\\celine_tran\\courses\\java310\\project1_tran_barba\\standardFormat_IK";
		String destination = "C:\\Users\\celine_tran\\courses\\java310\\project1_tran_barba\\normalizerOutput";
		Normalizer test2 = new Normalizer(source,destination);
		ArrayList<String> standardMovie = new ArrayList<String>();
		standardMovie.add("1979\tApocalypse Now\t153\tKaggle");
		ArrayList<String> resultArray = new ArrayList<String>();
		resultArray = test2.process(standardMovie);
		
		boolean actual = resultArray.contains("1979\tapocalypse now\t153\tKaggle");
		boolean excpected = true ;
		assertEquals(excpected,actual);
	}


@Test//this will test will test the normalizer if it keeps only first letter with different test case.
void NormalizeKeepFirstLetterTest() {
	String source = "C:\\Users\\celine_tran\\courses\\java310\\project1_tran_barba\\standardFormat_IK";
	String destination = "C:\\Users\\celine_tran\\courses\\java310\\project1_tran_barba\\normalizerOutput";
	Normalizer test3 = new Normalizer(source,destination);
	ArrayList<String> standardMovie = new ArrayList<String>();
	standardMovie.add("2009	The Hurt Locker	131 minutes	Kaggle");
	standardMovie.add("1979\tApocalypse Now\t153\tKaggle");
	ArrayList<String> resultArray = new ArrayList<String>();
	resultArray = test3.process(standardMovie);
	
	boolean actual = resultArray.contains("2009\tthe hurt locker\t131\tKaggle");
	boolean excpected = true ;
	assertEquals(excpected,actual);
}
}
